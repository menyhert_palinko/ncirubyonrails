# ncirubyonrails

NCI Ruby On Rails project (Book Store project)

- Coding approach: based on The Clean Code book by Robert C. Martin (Uncle Bob)
- KISS principle

Roles: Admin role and User role

Front end design based on Bootstrap.

Main focus on Server Side.

Development / Production parameters are separate in order to be able to host it properly on heroku.

Integrated with SendGrid on heroku to send emails out. There is no email sending in development mode.

Integrated with AWS S3 to store photos in the cloud only for production. In development mode the images are uploaded to the default path.

Integrated with Stripe to accept card payments

