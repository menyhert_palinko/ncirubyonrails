class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :token
      t.string :email
      t.text :log_message
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
