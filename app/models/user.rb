class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
  
  has_many :payment       
  
  REGEX_FIRST_LAST_NAME = /\A^[A-Z][A-Za-z\.\s\-]{2,24}$\z/i
  
  validates :first_name,
    format: { with: REGEX_FIRST_LAST_NAME }
    
  validates :last_name,
    format: { with: REGEX_FIRST_LAST_NAME }
  
  def full_name
    
    return "#{first_name} #{last_name}".strip if (first_name || last_name)
    "Unknown"
    
  end
  
end
