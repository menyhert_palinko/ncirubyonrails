class Book < ActiveRecord::Base
    
    #Validation mainly based on regex.
    #As it is an active record sql injection is not possible for the functions provided by rails
    #The validation performed here also protects against html/javascript injection
    
    mount_uploader :picture, PictureUploader
    
    REGEX_TITLE = /\A^(:?[A-Za-z][\w\s\-\.\&\+]*[\w]){2,32}$\z/i
    
    REGEX_DESCRIPTION = /\A^[\w][\w\s\-\.\!\?\,\(\)\'\"\&\*\@\+\#]{3,256}$\z/i
    
    REGEX_CATEGORY = /\A^[A-Za-z][A-Za-z\s]{2,32}$\z/i
    
    REGEX_PRICE = /\A^[0-9]+(?:\.[0-9]+)?$\z/i
    
    validates :title,
    uniqueness: { case_sensitive: false },
    format: { with: REGEX_TITLE }
    
    validates :description,
    format: { with: REGEX_DESCRIPTION }
    
    validates :category,
    format: { with: REGEX_CATEGORY }
    
    validates :price, 
    format: { with: REGEX_PRICE }
    
    validate :picture_size

    private

    def picture_size

        if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
        end

    end
    
    def self.search(search)
        #Separate parameter is being used so SQL injection is not possible  
	    where("title LIKE ?", "%#{search}%")
    end
end
