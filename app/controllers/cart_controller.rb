class CartController < ApplicationController
  
  before_filter :ensure_not_admin
  
  #Check if the currently logged in user is not Admin
  #If so, an error will be returned as cart is not an Admin option.
  def ensure_not_admin
    
    if current_user && current_user.admin?
    
      render :text => "Admin, Admin... What are you doing?", :status => :unauthorized
      
    end
    
  end
  
  def index
    
    if session[:cart] then
      
      @cart = session[:cart]
      
    else 
      
      @cart = {}
      
    end
    
  end
  
  def add

    id = params[:id]

    if session[:cart] then
      
      cart = session[:cart]
      
    else
      
      session[:cart] = {}
      cart = session[:cart]
      
    end
    
    if cart[id] then
      
      cart[id] = cart[id] + 1
      
    else
      
      cart[id] = 1
      
    end
    
    redirect_to :action => :index
    
  end
  
  def remove

    id = params[:id]

    if session[:cart] then
      
      cart = session[:cart]
      
    else
      
      session[:cart] = {}
      cart = session[:cart]
      
    end
    
    if cart[id] then
      
      cart[id] = cart[id] - 1
      
      if cart[id] <= 0 then
        
        cart.delete(id)
        
      end
      
    end
    
    redirect_to :action => :index
    
  end
    
  def removeAll
    
    session[:cart] = nil
    redirect_to :action => :index
    
  end

end
