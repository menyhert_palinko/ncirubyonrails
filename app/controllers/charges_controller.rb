class ChargesController < ApplicationController
    
    def new
        
        # Sum up the amount
        total = 0;
        
        session[:cart].each do |id,quantity |
            
            book = Book.find_by_id(id)
            total += quantity * book.price
            
        end
        
        #The amount is only used for displaying this time so multiplication is not required to cent format.
        @amountToDisplay = total
        
        #The amount has to be given in cents so multiplication is required
        #The system handles the amount for two decimal places
        
        if total > 0 then
            total *= 100;
        end
        
        @amount = total
    end
    
    def create
        
        if !session[:cart] || session[:cart].empty? then
            
            puts 'Payment failed. Cart is empty or session has been expired'
        
            render :new and return
            
        end
        
        # Build up the log message and sum up the amount
        logMessage = ""
        total = 0;
        
        session[:cart].each do |id,quantity |
            
            book = Book.find_by_id(id)
            
            if !logMessage.empty? then
                logMessage += ", "
            end
            
            logMessage += "Book title: " + book.title 
            logMessage += " - price: " + book.price.to_s + " EUR"
            logMessage += " - quantity: " + quantity.to_s + " piece(s)"
            
            total += quantity * book.price
            
        end
        
        #The amount has to be given in cents so multiplication is required
        #The system handles the amount for two decimal places
        
        if total > 0 then
            total *= 100;
        end
        
        @amount = total
    
        customer = Stripe::Customer.create(
            :email => params[:stripeEmail],
            :source  => params[:stripeToken]
        )
    
        charge = Stripe::Charge.create(
            :customer    => customer.id,
            :amount      => @amount.to_i,
            :description => 'NCI Ruby on Rails Stripe customer',
            :currency    => 'eur'
        )
        
        # Need to contruct and persist the custom payment object to track payments
        # Payment must contain the stripe token and be related to the user
        # This makes possible to track payments for users
        
        @payment = Payment.new({ 
            email: current_user.email,
            log_message: logMessage,
            token: params[:stripeToken], 
            user_id: current_user.id 
        })
        
        if !@payment.valid? then
            
            flash[:error] = "Please check registration errors" 
            puts 'Payment object is not valid'
            render :new and return
            
        end
        
        begin
        
            @payment.save
        
            rescue Exception => e
        
            flash[:error] = e.message
        
            puts 'Payment failed'
        
            render :new and return
        
        end
        
        flash[:success] = "Your payment was successful!"
        
        #empty the cart
        session[:cart] = nil
        @cart = nil
        
    end

end
