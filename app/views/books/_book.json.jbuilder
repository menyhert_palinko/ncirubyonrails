json.extract! book, :id, :title, :description, :price, :picture, :category, :created_at, :updated_at
json.url book_url(book, format: :json)
